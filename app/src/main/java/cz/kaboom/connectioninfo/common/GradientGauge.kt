package cz.kaboom.connectioninfo.common

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.View
import androidx.core.content.res.ResourcesCompat
import cz.kaboom.connectioninfo.R
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import kotlin.math.abs


class GradientGauge : View {
    //region Properties ====================================================
    var maxValue: Float = 50f
    var currentValue: Float = 0f
    var formatString: String = "%.2f MB/s"
    var gaugeWidth: Float = 70f
    var startAngle: Float = 45f
    var sweepAngle: Float = 270f
    //endregion ============================================================

    //region Fields ========================================================
    private val mGaugePaint: Paint = Paint()
    private val mTextPaint: Paint = Paint()
    private val mBoundsRect: RectF = RectF()
    private val mTextRect: Rect = Rect()
    private var mShader: Shader? = null
    private var mCurrDrawAngle: Float = 0f
    private var mAnimationDelay: Long = 2
    private var mAnimationStep: Float = 0.3f
    private var mTimerSubscription: Disposable? = null
    //endregion ============================================================

    //region Constructors ==================================================
    constructor(context: Context?) : super(context)

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )
    //endregion ============================================================

    //region Init ==========================================================
    init {
        mTextPaint.color = Color.parseColor("#BB86FC")
        mTextPaint.isAntiAlias = true
        try {
            mTextPaint.typeface = ResourcesCompat.getFont(context, R.font.roboto)
        } catch (e: Exception) {
            Log.e(Const.LOG_TAG, "Creating fonts error: ${e.localizedMessage}")
            mTextPaint.typeface = Typeface.SANS_SERIF
        }


        mGaugePaint.color = Color.DKGRAY
        mGaugePaint.style = Paint.Style.STROKE
        mGaugePaint.strokeCap = Paint.Cap.ROUND
        mGaugePaint.strokeWidth = gaugeWidth
        mGaugePaint.isAntiAlias = true

        mTimerSubscription = Observable.interval(mAnimationDelay, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { onTimer() }
    }
    //endregion ============================================================

    //region Methods =======================================================
    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        val size =
            (if (this.width < this.height) this.width else this.height).toFloat() - gaugeWidth * 2
        mBoundsRect.set(
            this.width / 2 - size / 2,
            this.height / 2 - size / 2,
            this.width / 2 + size / 2,
            this.height / 2 + size / 2
        )
        mShader = SweepGradient(
            this.width / 2.toFloat(),
            this.height / 2.toFloat(),
            intArrayOf(Color.RED, Color.YELLOW, Color.GREEN),
            null
        )
        mTextPaint.textSize = this.height / 10.toFloat()
    }
    
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        drawText(canvas)
        drawGauge(canvas)
    }
    
    private fun drawText(canvas: Canvas) {
        val caption = String.format(formatString, currentValue)
        mTextPaint.getTextBounds(caption, 0, caption.length, mTextRect)
        canvas.drawText(
            caption,
            this.width / 2 - mTextRect.width() / 2.toFloat(),
            this.height / 2 + mTextRect.height() / 2.toFloat(),
            mTextPaint
        )
    }
    
    private fun drawGauge(canvas: Canvas) {
        canvas.save()
        canvas.rotate(90f, this.width / 2.toFloat(), this.height / 2.toFloat())

        mGaugePaint.shader = null
        mGaugePaint.alpha = 50
        canvas.drawArc(mBoundsRect, startAngle, sweepAngle, false, mGaugePaint)
        mGaugePaint.shader = mShader
        mGaugePaint.alpha = 1000
        if (mCurrDrawAngle > mAnimationStep) canvas.drawArc(
            mBoundsRect,
            startAngle,
            mCurrDrawAngle,
            false,
            mGaugePaint
        )

        canvas.restore()
    }
    
    private fun onTimer() {
        val tmpAngle =
            (sweepAngle / maxValue) * if (currentValue > maxValue) maxValue else currentValue
        if (abs(tmpAngle - mCurrDrawAngle) <= mAnimationStep && currentValue == 0f) return
        if (mCurrDrawAngle < tmpAngle) mCurrDrawAngle += mAnimationStep else mCurrDrawAngle -= mAnimationStep
        invalidate()
    }
    //endregion ============================================================
}