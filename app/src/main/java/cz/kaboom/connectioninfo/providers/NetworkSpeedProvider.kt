package cz.kaboom.connectioninfo.providers

import android.content.Context
import androidx.lifecycle.LiveData
import cz.kaboom.connectioninfo.R
import cz.kaboom.connectioninfo.common.Const
import fr.bmartel.speedtest.SpeedTestReport
import fr.bmartel.speedtest.SpeedTestSocket
import fr.bmartel.speedtest.inter.ISpeedTestListener
import fr.bmartel.speedtest.model.SpeedTestError
import fr.bmartel.speedtest.utils.SpeedTestUtils
import org.koin.core.KoinComponent
import java.math.BigDecimal

enum class SpeedTestDirection { IDLE, DOWNLOAD, UPLOAD }

class NetworkSpeedProvider(
    private val context: Context,
    private val speedtestSocket: SpeedTestSocket
) : LiveData<SpeedTestEvent>(), KoinComponent {

    private var direction = SpeedTestDirection.IDLE
    private var packetCounter: Int = 0
    private val idle: Boolean get() = this.direction == SpeedTestDirection.IDLE


    fun switchState() {
        if (idle) start() else stop()
    }

    private fun start() {
        direction = SpeedTestDirection.DOWNLOAD
        runTest()
        postValue(SpeedTestEvent.State(true))
    }

    private fun stop() {
        if (direction != SpeedTestDirection.IDLE) {
            direction = SpeedTestDirection.IDLE
            speedtestSocket.forceStopTask()
        }
    }

    private fun runTest() {
        speedtestSocket.clearListeners()
        speedtestSocket.addSpeedTestListener(SpeedTestListener())
        packetCounter = 0
        val downloadURL = Const.SPEED_TEST_DOWNLOAD_URL
        val uploadURL = Const.SPEED_TEST_UPLOAD_URL + SpeedTestUtils.generateFileName() + context.getString(R.string.ul_file_ext)
        when (direction) {
            SpeedTestDirection.DOWNLOAD -> speedtestSocket.startDownload(downloadURL)
            SpeedTestDirection.UPLOAD -> speedtestSocket.startUpload(uploadURL, Const.UPLOAD_FILE_SIZE)
            SpeedTestDirection.IDLE -> return
        }
    }

    inner class SpeedTestListener : ISpeedTestListener {

        override fun onProgress(percent: Float, report: SpeedTestReport?) {
            if (++packetCounter <= Const.SKIP_PACKETS_COUNT) return
            postValue(SpeedTestEvent.Data(direction, percent, report?.transferRateBit!!))
        }

        override fun onCompletion(report: SpeedTestReport?) {
            if (direction == SpeedTestDirection.DOWNLOAD) {
                direction = SpeedTestDirection.UPLOAD
                runTest()
                return
            }
            direction = SpeedTestDirection.IDLE
            postValue(SpeedTestEvent.State(false))
        }

        override fun onError(speedTestError: SpeedTestError?, errorMessage: String?) {
            speedtestSocket.clearListeners()
            direction = SpeedTestDirection.IDLE
            postValue(SpeedTestEvent.Error(Throwable(errorMessage)))
        }
    }
}

sealed class SpeedTestEvent{
    data class State(val started: Boolean): SpeedTestEvent()
    data class Data(
        val direction: SpeedTestDirection,
        val percent: Float,
        val bitsPerSec: BigDecimal
    ): SpeedTestEvent()
    data class Error(val error: Throwable): SpeedTestEvent()
}
