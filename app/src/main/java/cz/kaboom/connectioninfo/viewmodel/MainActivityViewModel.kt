package cz.kaboom.connectioninfo.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.kaboom.connectioninfo.providers.InternetStateProvider
import cz.kaboom.connectioninfo.providers.NetworkInfoProvider
import cz.kaboom.connectioninfo.providers.NetworkSpeedProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent

class MainActivityViewModel(
    val internetStateProvider: InternetStateProvider,
    val networkInfoProvider: NetworkInfoProvider,
    val networkSpeedProvider: NetworkSpeedProvider

) : ViewModel(), KoinComponent{

    fun getNetworkInfo(){
        viewModelScope.launch(Dispatchers.IO) { networkInfoProvider.networkLookup() }
    }
}